# Talkdesk Challenge

This project was developed in the context of a [Talkdesk](https://talkdesk.com/) challenge and is powered by [Twilio](https://twilio.com/). A demo application is available [here](https://obscure-coast-12560.herokuapp.com), but using a trial account which includes some restrictions:

- Phone to browser and browser to phone calls are limited to my personal phone number.
- Free balance to experiment with is limited and should run out soon.

The demo application is also running on a free [Heroku](https://www.heroku.com/) account, and it might take a couple of minutes to load if the dynos were asleep due to inactivity.

## Starting Guide

This application looks and feels like most mobile phone applications and using it should come natural. Both the user interface and the keyboard can be used to dial a phone number, but the `+` symbol can only be dialed through the keyboard.

Besides the obvious numbered keys, the following keys are also binded:

- <kbd>Enter</kbd>: Makes a new call or accepts an incoming one.
- <kbd>Esc</kbd>: Ends the current call or declines an incoming one.
- <kbd>Backspace</kbd>: Deletes the last dialed phone number.

Givens the limitations mentioned above and a know issue noted below, only browser to browser phone calls can be established by people other than myself. Regrettably, though, the user interface was not built with that idea in mind and a workaround was put in place.

Here's how to easily make browser to browser phone calls:

1. Open the demo application mentioned above on 2 different browser windows.
2. Take note of the _Twilio Identity_ on the top-right corner of one of the windows (e.g. `IgnominiousOwenGunsight`).
3. Open the developer tools on the other window (usually with the <kbd>F12</kbd> key or <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>I</kbd> on Windows/Linux and <kbd>Cmd</kbd>+<kbd>Opt</kbd>+<kbd>I</kbd> on OS X).
4. Type `Dialer.setPhoneNumber("TwilioIdentityHere")` on the developer tools console and press <kbd>Enter</kbd>.
5. Close the developer tools and press the <kbd>Call</kbd> button to make a new call.

## Technologies

This application was built on core technologies such as [React](https://facebook.github.io/react/) - a JavaScript library for building user interfaces - and [TypeScript](https://www.typescriptlang.org/) - a typed superset of JavaScript that compiles to plain JavaScript.

Many other tools and libraries were used during development to achieve the final solution:

- [Axios](https://github.com/mzabriskie/axios): Promise based HTTP client for the browser and Node.js.
- [CSS Modules](https://github.com/css-modules/css-modules): Modular and reusable CSS.
- [CSSNext](http://cssnext.io/): PostCSS plugin which helps use the latest CSS syntax today.
- [Enzyme](http://airbnb.io/enzyme/): JavaScript Testing utilities for React.
- [Jest](https://facebook.github.io/jest/): Painless JavaScript testing framework.
- [Material-UI](http://www.material-ui.com/): A Set of React Components that Implement Google's Material Design.
- [Stylelint](http://stylelint.io/): A mighty, modern CSS linter which helps enforce consistent conventions and avoid errors in stylesheets.
- [TSLint](https://palantir.github.io/tslint/): An extensible linter for the TypeScript language.
- [Webpack](https://webpack.js.org/): Module bundler for modern JavaScript applications.

## Known Issues

- Development was more focused on the client side and a base Node.js application from Twilio was used for the server side. Unfortunately this is just a "starter" server application which is not ready to accept incoming connections from a real phone.

## License

Use of this source code is governed by an MIT-style license that can be found in the [LICENSE](LICENSE) file.
