/**
 * Module declaration for CSS files.
 */
declare module "*.css";

/**
 * Module declaration for JPG files (with `image-size-loader` support).
 */
declare module "*.jpg" {
    let metadata: {
        bytes: number;
        height: number;
        src: string;
        type: string;
        width: number;
        toString(): string;
    };

    export = metadata;
}

/**
 * Module declaration for PNG files (with `image-size-loader` support).
 */
declare module "*.png" {
    let metadata: {
        bytes: number;
        height: number;
        src: string;
        type: string;
        width: number;
        toString(): string;
    };

    export = metadata;
}
