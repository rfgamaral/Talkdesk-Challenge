/**
 * Variable declaration for the Node.Js global variable.
 */
declare var global: NodeJS.Global;

/**
 * Namespace declaration for the Node.js environment with declarations for global libraries.
 */
declare namespace NodeJS {

    interface Global {
        Twilio: {
            Device: Partial<Twilio.IDevice>;
        }
    }

}
