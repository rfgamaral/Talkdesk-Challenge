const path = require('path');
const webpack = require('webpack');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const StylelintPlugin = require('stylelint-webpack-plugin');

/**
 * Constant properties for the Webpack 2 configuration below.
 */
const CONFIGURATION = {
    path: {
        application: path.join(__dirname, '..', 'app'),
        assets: path.join(__dirname, '..', 'app', 'assets'),
        configuration: path.join(__dirname, '..', 'config'),
        distribution: path.join(__dirname, '..', 'build', 'dist'),
        nodeModules: path.join(__dirname, '..', 'node_modules')
    },

    template: {
        fontFilename: 'assets/fonts/[name].[hash:8].[ext]',
        imageFilename: 'assets/images/[name].[hash:8].[ext]',
        scriptFilename: 'scripts/[name].[hash:8].js',
        styleClassname: '[name]_[local]_[hash:base64:5]',
        styleFilename: 'assets/styles/[name].[hash:8].css'
    }
};

/**
 * Webpack 2 configuration for the `browser-telephone` project.
 */
module.exports = {
    context: path.join(__dirname, '..'),

    entry: {
        app: [
            'react-hot-loader/patch',
            './app/Index.tsx'
        ],
        vendor: [
            'axios',
            'lodash',
            'material-ui',
            'react',
            'react-css-modules',
            'react-dom',
            'react-event-listener',
            'react-tap-event-plugin'
        ]
    },

    output: {
        path: CONFIGURATION.path.distribution,
        filename: CONFIGURATION.template.scriptFilename,
        pathinfo: true
    },

    devtool: 'cheap-module-source-map',

    devServer: {
        hot: true,
        stats: {
            chunks: false,
            hash: false,
        }
    },

    resolve: {
        extensions: ['.js', 'jsx', '.json', '.ts', '.tsx'],
        modules: [
            CONFIGURATION.path.application,
            CONFIGURATION.path.nodeModules
        ]
    },

    module: {
        rules: [
            {
                include: CONFIGURATION.path.application,
                test: /\.tsx?$/,
                loaders: [
                    'react-hot-loader/webpack',
                    'awesome-typescript-loader'
                ]
            },
            {
                include: CONFIGURATION.path.application,
                test: /\.tsx?$/,
                loader: 'tslint-loader',
                options: {
                    configFile: path.join(CONFIGURATION.path.configuration, 'tslint.json')
                }
            },
            {
                include: CONFIGURATION.path.application,
                enforce: 'pre',
                test: /\.js$/,
                loader: 'source-map-loader'
            },
            {
                include: CONFIGURATION.path.application,
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                context: '.',
                                modules: true,
                                importLoaders: 1,
                                camelCase: true,
                                sourceMap: true,
                                localIdentName: CONFIGURATION.template.styleClassname
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: 'inline',
                                plugins: () => [
                                    require('postcss-import'),
                                    require('postcss-cssnext')
                                ]
                            }
                        }
                    ]
                })
            },
            {
                include: CONFIGURATION.path.application,
                test: /\.(gif|jpg|png)$/,
                loader: 'image-size-loader',
                options: {
                    name: CONFIGURATION.template.imageFilename
                }
            },
            {
                include: CONFIGURATION.path.application,
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader',
                options: {
                    name: CONFIGURATION.template.imageFilename,
                    limit: 10000,
                    mimetype: 'image/svg+xml'
                }
            },
            {
                include: CONFIGURATION.path.application,
                test: /\.woff(2)?(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader',
                options: {
                    name: CONFIGURATION.template.fontFilename,
                    limit: 10000,
                    mimetype: 'application/font-woff'
                }
            },
            {
                include: CONFIGURATION.path.application,
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader',
                options: {
                    name: CONFIGURATION.template.fontFilename,
                    limit: 10000,
                    mimetype: 'application/octet-stream'
                }
            },
            {
                include: CONFIGURATION.path.application,
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file-loader',
                options: {
                    name: CONFIGURATION.template.fontFilename
                }
            }
        ]
    },

    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            filename: CONFIGURATION.template.scriptFilename,
            name: 'vendor'
        }),
        new ExtractTextPlugin({
            filename: CONFIGURATION.template.styleFilename,
            allChunks: true
        }),
        new HtmlPlugin({
            template: path.join(CONFIGURATION.path.assets, 'index.html'),
            favicon: path.join(CONFIGURATION.path.assets, 'favicon.ico'),
            inject: 'body'
        }),
        new StylelintPlugin({
            configFile: path.join(CONFIGURATION.path.configuration, 'stylelint.json'),
            files: ['app/**/*.css']
        })
    ]
};
