const path = require('path');
var imageSize = require('image-size');
var fs = require('fs');

/**
 * Load information for image files before they are run with Jest.
 */
module.exports = {
    process: (src, filename, config, options) => {
        var dimensions = imageSize(filename);

        let imageFile = {
            width: dimensions.width,
            height: dimensions.height,
            type: dimensions.type,
            src: path.relative(path.join(config.rootDir, 'app'), filename),
            bytes: fs.statSync(filename).size
        };

        return 'module.exports = ' + JSON.stringify(imageFile) + ';';
    }
};
