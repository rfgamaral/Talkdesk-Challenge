import * as React from "react";
import * as CSSModules from "react-css-modules";

import Axios, { AxiosInstance } from "axios";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

import Paper from "material-ui/Paper";
import Snackbar from "material-ui/Snackbar";

import Dialer from "./dialer/Dialer";

import * as logo from "assets/images/logo.png";

interface IApplicationState {
    snackbarAction: string;
    snackbarAutoHideDuration: number;
    snackbarMessage: string;
    snackbarOpen: boolean;
    twilioDeviceReady: boolean;
    twilioIdentity: string;
}

@CSSModules(require<any>("./Application.css"))
export default class Application extends React.Component<any, Partial<IApplicationState>> {

    private readonly paperStyle: any = {
        alignItems: "center",
        display: "flex",
        fontSize: 24,
        height: 496,
        justifyContent: "center",
        padding: 10
    };

    private readonly snackbarStyle: any = {
        whiteSpace: "nowrap",
        width: "100%"
    };

    private readonly snackbarContentStyle: any = {
        fontSize: 16
    };

    private readonly snackbarBodyStyle: any = {
        margin: "0 auto",
        maxWidth: "none"
    };

    private readonly twilioServer: AxiosInstance = Axios.create({
        baseURL: "https://infinite-waters-25070.herokuapp.com"
    });

    public constructor() {
        super();

        this.state = {
            snackbarAction: "",
            snackbarAutoHideDuration: 4000,
            snackbarMessage: "",
            snackbarOpen: false,
            twilioDeviceReady: false,
            twilioIdentity: ""
        };

        this.handleSnackbarRequestClose = this.handleSnackbarRequestClose.bind(this);

        Twilio.Device.ready(this.handleTwilioDeviceReady.bind(this));
        Twilio.Device.offline(this.handleTwilioOfflineReady.bind(this));
        Twilio.Device.error(this.handleTwilioDeviceError.bind(this));
    }

    public render(): JSX.Element {
        const twilioIdentityElement = this.state.twilioDeviceReady && (
            <div styleName="identity">
                <strong>Twilio Identity</strong>
                <br />
                {this.state.twilioIdentity}
            </div>
        );

        const mainContentElement = this.state.twilioDeviceReady ? <Dialer /> : (
            <Paper
                zDepth={1}
                style={this.paperStyle}>
                Initializing...
            </Paper>
        );

        return (
            <MuiThemeProvider>
                <div>
                    <div styleName="header">
                        <img
                            src={logo.src}
                            width={logo.width}
                            height={logo.height} />
                        {twilioIdentityElement}
                    </div>
                    {mainContentElement}
                    <Snackbar
                        action={this.state.snackbarAction}
                        autoHideDuration={this.state.snackbarAutoHideDuration}
                        bodyStyle={this.snackbarBodyStyle}
                        contentStyle={this.snackbarContentStyle}
                        message={this.state.snackbarMessage}
                        onActionTouchTap={this.handleSnackbarActionTouchTap}
                        onRequestClose={this.handleSnackbarRequestClose}
                        open={this.state.snackbarOpen}
                        style={this.snackbarStyle} />
                </div>
            </MuiThemeProvider>
        );
    }

    public componentDidMount(): Promise<any> {
        return this.requestTwilioClientToken();
    }

    private handleSnackbarRequestClose(reason: string): void {
        this.setState({
            snackbarAction: "",
            snackbarMessage: "",
            snackbarOpen: false
        });
    }

    private handleSnackbarActionTouchTap(event: any): void {
        if (event.target.textContent === "reload") {
            window.location.reload();
        }
    }

    private handleTwilioDeviceReady(device: Twilio.IDevice): void {
        this.setState({
            twilioDeviceReady: true
        });
    }

    private handleTwilioOfflineReady(device: Twilio.IDevice): void {
        this.requestTwilioClientToken();
    }

    private handleTwilioDeviceError(error: Twilio.IError): void {
        this.setState({
            snackbarMessage: `ERROR: (${error.code}) ${error.message}.`,
            snackbarOpen: true
        });
    }

    private requestTwilioClientToken(): Promise<any> {
        return this.twilioServer.get("/token")
            .then((response) => {
                Twilio.Device.setup(response.data.token, {
                    closeProtection: true
                });

                this.setState({
                    twilioIdentity: response.data.identity
                });
            })
            .catch((error) => {
                this.setState({
                    snackbarAction: "reload",
                    snackbarMessage: "ERROR: Could not get a token from the server!",
                    snackbarOpen: true
                });
            });
    }

}
