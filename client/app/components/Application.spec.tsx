import * as React from "react";

import Axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import { mount, shallow } from "enzyme";

import Application from "components/Application";

describe("Application", () => {

    let axiosCreateSpy: jest.SpyInstance<(config?: AxiosRequestConfig) => AxiosInstance>;

    beforeAll(() => {
        axiosCreateSpy = jest.spyOn(Axios, "create");

        global.Twilio = {
             Device: {
                 error: jest.fn(),
                 offline: jest.fn(),
                 ready: jest.fn(),
                 setup: jest.fn()
             }
        };
    });

    describe("#initialization", () => {

        test("twilio server instance is initialized with correct backend endpoint", () => {
            axiosCreateSpy.mockImplementation(() => ({
                get: () => new Promise(() => { /* noop */ })
            }));

            shallow(<Application />);

            expect(axiosCreateSpy).toHaveBeenCalledWith({
                baseURL: "https://infinite-waters-25070.herokuapp.com"
            });
        });

        test("twilio server network request is made with correct relative URL", () => {
            axiosCreateSpy.mockImplementation(() => ({
                get: jest.fn(() => new Promise(() => { /* noop */ }))
            }));

            const application: any = mount(<Application />);

            expect(application.instance().twilioServer.get).toHaveBeenCalledWith("/token");
        });

        describe("when making a network request to twilio server", () => {

            describe("and the operation completed successfully", () => {

                test("twilio device is initialized with the token from the response", async () => {
                    axiosCreateSpy.mockImplementation(() => ({
                        get: () => Promise.resolve({
                            data: {
                                token: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9"
                            }
                        })
                    }));

                    const application: any = shallow(<Application />);
                    await application.instance().componentDidMount();

                    expect(Twilio.Device.setup).toHaveBeenCalledWith("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9", {
                        closeProtection: true
                    });
                });

                test("twilio device is initialized with correct options", async () => {
                    axiosCreateSpy.mockImplementation(() => ({
                        get: () => Promise.resolve({
                            data: {
                                token: ""
                            }
                        })
                    }));

                    const application: any = shallow(<Application />);
                    await application.instance().componentDidMount();

                    expect(Twilio.Device.setup).toHaveBeenCalledWith(expect.anything(), {
                        closeProtection: true
                    });
                });

                test("twilio identity state is updated with the identity from the response", async () => {
                    axiosCreateSpy.mockImplementation(() => ({
                        get: () => Promise.resolve({
                            data: {
                                identity: "YawningZeldaOakland"
                            }
                        })
                    }));

                    const application: any = shallow(<Application />);
                    await application.instance().componentDidMount();

                    expect(application.state().twilioIdentity).toEqual("YawningZeldaOakland");
                });

            });

            describe("and the operation failed", () => {

                test("snackbar state is updated with an error message", async () => {
                    axiosCreateSpy.mockImplementation(() => ({
                        get: () => Promise.reject(undefined)
                    }));

                    const application: any = await shallow(<Application />);
                    await application.instance().componentDidMount();

                    expect(application.state()).toEqual(expect.objectContaining({
                        snackbarAction: "reload",
                        snackbarMessage: "ERROR: Could not get a token from the server!",
                        snackbarOpen: true
                    }));
                });

            });

        });

    });

    describe("#render", () => {

        test("application before twilio device is initialized renders correctly", () => {
            expect(shallow(<Application />)).toMatchSnapshot();
        });

        test("application after twilio device is successfully initialized renders correctly", () => {
            const application: any = shallow(<Application />);

            application.setState({
                twilioDeviceReady: true,
                twilioIdentity: "YawningZeldaOakland"
            });

            expect(application).toMatchSnapshot();
        });

        test("application with error message on twilio device initialization renders correctly", () => {
            const application: any = shallow(<Application />);

            application.setState({
                snackbarAction: "reload",
                snackbarMessage: "ERROR: Could not get a token from the server!",
                snackbarOpen: true
            });

            expect(application).toMatchSnapshot();
        });

    });

});
