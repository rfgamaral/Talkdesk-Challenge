import * as React from "react";

import { shallow } from "enzyme";

import CallButton from "components/dialer/call-button/CallButton";

describe("CallButton", () => {

    describe("#render", () => {

        test("answer button renders correctly", () => {
            expect(shallow(
                <CallButton
                    type="answer"
                    onClick={undefined} />
            )).toMatchSnapshot();
        });

        test("decline button renders correctly", () => {
            expect(shallow(
                <CallButton
                    type="decline"
                    onClick={undefined} />
            )).toMatchSnapshot();
        });

        test("call button renders correctly", () => {
            expect(shallow(
                <CallButton
                    type="call"
                    onClick={undefined} />
            )).toMatchSnapshot();
        });

        test("end-call button renders correctly", () => {
            expect(shallow(
                <CallButton
                    type="end-call"
                    onClick={undefined} />
            )).toMatchSnapshot();
        });

    });

    describe("#onTouchTap", () => {

        test("event handler was called with expected arguments", () => {
            const onClickStub = jest.fn();

            const callButton = shallow(
                <CallButton
                    type="answer"
                    onClick={onClickStub} />
            );

            callButton.simulate("touchTap");

            expect(onClickStub).toHaveBeenCalledWith("answer");
        });

    });

});
