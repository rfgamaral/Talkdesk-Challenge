import * as _ from "lodash";
import * as React from "react";

import RaisedButton from "material-ui/RaisedButton";

import CallIcon from "material-ui/svg-icons/communication/call";
import CallEndIcon from "material-ui/svg-icons/communication/call-end";

import { fullWhite, greenA700, redA700 } from "material-ui/styles/colors";

interface ICallButtonProperties {
    type: CallButtonType;
    onClick(buttonType: CallButtonType): void;
}

export enum CallButtonType {
    Answer = "answer",
    Call = "call",
    Decline = "decline",
    EndCall = "end-call"
}

export default class CallButton extends React.PureComponent<ICallButtonProperties, any> {

    private readonly callButtonStyle: any = {
        height: 64,
        width: "100%"
    };

    private readonly callButtonLabelStyle: any = {
        color: fullWhite,
        fontSize: 24
    };

    private readonly callButtonIconStyle: any = {
        height: 44,
        marginTop: -5,
        width: 44
    };

    public constructor(props: ICallButtonProperties) {
        super(props);

        this.handleButtonTouchTap = this.handleButtonTouchTap.bind(this);
    }

    public render(): JSX.Element {
        let callButtonBackgroundColor;
        let callButtonIconElement;

        if (this.props.type === CallButtonType.Decline || this.props.type === CallButtonType.EndCall) {
            callButtonBackgroundColor = redA700;
            callButtonIconElement = (
                <CallEndIcon
                    color={fullWhite}
                    style={this.callButtonIconStyle} />
            );
        } else {
            callButtonBackgroundColor = greenA700;
            callButtonIconElement = (
                <CallIcon
                    color={fullWhite}
                    style={this.callButtonIconStyle} />
            );
        }

        return (
            <RaisedButton
                backgroundColor={callButtonBackgroundColor}
                label={_.startCase(this.props.type)}
                labelStyle={this.callButtonLabelStyle}
                icon={callButtonIconElement}
                onTouchTap={this.handleButtonTouchTap}
                style={this.callButtonStyle} />
        );
    }

    private handleButtonTouchTap(event: any): void {
        this.props.onClick(this.props.type);
    }

}
