import * as _ from "lodash";
import * as React from "react";
import * as CSSModules from "react-css-modules";

import EventListener from "react-event-listener";

import Paper from "material-ui/Paper";

import { DIALPAD_DIGITS } from "constants/dialer/DialpadDigits";
import { KeyCodes } from "constants/events/KeyCodes";

import CallButton, { CallButtonType } from "./call-button/CallButton";
import CallCard, { CallStatusMode } from "./call-card/CallCard";
import Dialpad from "./dialpad/Dialpad";
import PhoneNumber from "./phone-number/PhoneNumber";

interface IDialerState {
    callStatus: CallStatusMode;
    cardName: string;
    cardPhoneNumber: string;
    phoneNumber: string;
}

@CSSModules(require<any>("./Dialer.css"))
export default class Dialer extends React.Component<any, IDialerState> {

    private readonly dialerStyle: any = {
        padding: 10
    };

    private activeConnection: any;

    public constructor() {
        super();

        this.state = {
            callStatus: "idle",
            cardName: undefined,
            cardPhoneNumber: undefined,
            phoneNumber: ""
        };

        this.handleDocumentKeyDown = this.handleDocumentKeyDown.bind(this);
        this.handlePhoneNumberDeleteClick = this.handlePhoneNumberDeleteClick.bind(this);
        this.handleDialpadDial = this.handleDialpadDial.bind(this);
        this.handleCallButtonClick = this.handleCallButtonClick.bind(this);

        Twilio.Device.connect(this.handleTwilioDeviceConnect.bind(this));
        Twilio.Device.disconnect(this.handleTwilioDeviceDisconnect.bind(this));
        Twilio.Device.incoming(this.handleTwilioDeviceIncoming.bind(this));
        Twilio.Device.cancel(this.handleTwilioDeviceCancel.bind(this));
    }

    public render(): JSX.Element {
        return (
            <Paper
                zDepth={1}
                style={this.dialerStyle}>
                <EventListener
                    target="document"
                    onKeyDown={this.handleDocumentKeyDown} />
                {this.renderMainDialerContentElement()}
                <div styleName="dial-buttons">
                    {this.renderDialButtonsElement()}
                </div>
            </Paper>
        );
    }

    public componentDidMount(): void {
        // Globally expose function for debugging purposes only!

        (window as any).Dialer = {
            setPhoneNumber: (phoneNumber: string) => {
                this.setState({ phoneNumber });
            }
        };
    }

    // TODO: Move this code to render()!

    private renderMainDialerContentElement(): JSX.Element {
        switch (this.state.callStatus) {
            case "idle":
                return (
                    <div>
                        <PhoneNumber
                            onDeleteClick={this.handlePhoneNumberDeleteClick}>
                            {this.state.phoneNumber}
                        </PhoneNumber>
                        <Dialpad
                            onDial={this.handleDialpadDial} />
                    </div>
                );

            default:
                return (
                    <CallCard
                        name={this.state.cardName}
                        phoneNumber={this.state.cardPhoneNumber}
                        status={this.state.callStatus} />
                );
        }
    }

    // TODO: Move this code to render()!

    private renderDialButtonsElement(): JSX.Element {
        switch (this.state.callStatus) {
            case "incoming-call":
                return (
                    <div styleName="incoming-call">
                        <CallButton
                            onClick={this.handleCallButtonClick}
                            type={CallButtonType.Answer} />
                        <CallButton
                            onClick={this.handleCallButtonClick}
                            type={CallButtonType.Decline} />
                    </div>
                );

            case "active":
            case "calling":
                return (
                    <CallButton
                        onClick={this.handleCallButtonClick}
                        type={CallButtonType.EndCall} />
                );

            default:
                return (
                    <CallButton
                        onClick={this.handleCallButtonClick}
                        type={CallButtonType.Call} />
                );

        }
    }

    private handleDocumentKeyDown(event: any): void {
        if (event.keyCode === KeyCodes.Enter) {
            switch (this.state.callStatus) {
                case "idle":
                    this.establishNewCall();
                    break;

                case "incoming-call":
                    this.answerIncomingCall();
                    break;

                default:
                    return;
            }
        }

        if (event.keyCode === KeyCodes.Esc) {
            switch (this.state.callStatus) {
                case "active":
                case "calling":
                    this.terminateActiveCall();
                    break;

                case "incoming-call":
                    this.declineIncomingCall();
                    break;

                default:
                    return;
            }
        }

        if (this.state.callStatus === "idle") {
            if (event.keyCode === KeyCodes.Backspace) {
                this.handlePhoneNumberDeleteClick();
            }

            if (_.includes(_.map(DIALPAD_DIGITS), event.key)) {
                this.handleDialpadDial(event.key);
            }
        }
    }

    private handlePhoneNumberDeleteClick(): void {
        if (this.state.phoneNumber === "") {
            return;
        }

        this.setState({
            phoneNumber: this.state.phoneNumber.slice(0, -1)
        });
    }

    private handleDialpadDial(digit: string): void {
        this.setState({
            phoneNumber: this.state.phoneNumber + digit
        });
    }

    private handleCallButtonClick(buttonType: CallButtonType): void {
        switch (buttonType) {
            case CallButtonType.Answer:
                this.answerIncomingCall();
                break;

            case CallButtonType.Decline:
                this.declineIncomingCall();
                break;

            case CallButtonType.EndCall:
                this.terminateActiveCall();
                break;

            default:
                this.establishNewCall();
        }
    }

    private handleTwilioDeviceConnect(connection: Twilio.IConnection): void {
        this.setState({
            callStatus: "active"
        });
    }

    private handleTwilioDeviceDisconnect(connection: Twilio.IConnection): void {
        this.setState({
            callStatus: "idle"
        });
    }

    private handleTwilioDeviceIncoming(connection: Twilio.IConnection): void {
        this.activeConnection = connection;

        this.setState({
            callStatus: "incoming-call",
            cardPhoneNumber: connection.parameters.From
        });
    }

    private handleTwilioDeviceCancel(connection: Twilio.IConnection): void {
        this.setState({
            callStatus: "idle"
        });
    }

    private answerIncomingCall(): void {
        this.activeConnection.accept();
    }

    private declineIncomingCall(): void {
        this.activeConnection.reject();

        this.setState({
            callStatus: "idle"
        });
    }

    private terminateActiveCall(): void {
        this.activeConnection.disconnect();
    }

    private establishNewCall(): void {
        if (this.state.phoneNumber === "") {
            return;
        }

        this.activeConnection = Twilio.Device.connect({
            To: this.state.phoneNumber
        });

        this.setState({
            callStatus: "calling",
            cardPhoneNumber: this.state.phoneNumber
        });
    }

}
