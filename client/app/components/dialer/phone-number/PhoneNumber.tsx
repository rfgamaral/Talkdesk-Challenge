import * as React from "react";
import * as CSSModules from "react-css-modules";

import FlatButton from "material-ui/FlatButton";
import BackspaceIcon from "material-ui/svg-icons/content/backspace";

import { cyan900, grey600 } from "material-ui/styles/colors";

interface IPhoneNumberProperties {
    onDeleteClick(): void;
}

@CSSModules(require<any>("./PhoneNumber.css"))
export default class PhoneNumber extends React.Component<IPhoneNumberProperties, any> {

    private readonly phoneNumberItemLeftStyle: any = {
        color: cyan900
    };

    public constructor() {
        super();

        this.handleBackspaceTouchTap = this.handleBackspaceTouchTap.bind(this);
    }

    public render(): JSX.Element {
        return (
            <div styleName="container">
                <div styleName="item-left" style={this.phoneNumberItemLeftStyle}>
                    {this.props.children}
                </div>
                <div styleName="item-right">
                    <FlatButton
                        icon={<BackspaceIcon color={grey600} />}
                        onTouchTap={this.handleBackspaceTouchTap} />
                </div>
            </div>
        );
    }

    private handleBackspaceTouchTap(event: any): void {
        this.props.onDeleteClick();
    }

}
