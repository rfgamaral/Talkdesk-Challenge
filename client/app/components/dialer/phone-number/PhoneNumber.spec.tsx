import * as React from "react";

import { shallow } from "enzyme";

import FlatButton from "material-ui/FlatButton";

import PhoneNumber from "components/dialer/phone-number/PhoneNumber";

describe("PhoneNumber", () => {

    describe("#render", () => {

        test("phone number with delete button renders correctly", () => {
            expect(shallow(
                <PhoneNumber
                    onDeleteClick={undefined}>
                    (866) 740-4531
                </PhoneNumber>
            )).toMatchSnapshot();
        });

    });

    describe("#onTouchTap", () => {

        test("backspace button event handler was called", () => {
            const onDeleteClickStub = jest.fn();

            const backspaceButton = shallow(
                <PhoneNumber onDeleteClick={onDeleteClickStub} />
            ).find(FlatButton);

            backspaceButton.simulate("touchTap");

            expect(onDeleteClickStub).toHaveBeenCalled();
        });

    });

});
