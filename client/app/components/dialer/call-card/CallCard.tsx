import * as _ from "lodash";
import * as React from "react";
import * as CSSModules from "react-css-modules";
import * as DateTime from "utils/DateTime";

import { CONTACTS } from "constants/data/Contacts";

import Paper from "material-ui/Paper";

import { fullWhite } from "material-ui/styles/colors";

interface ICallCardProperties {
    name?: string;
    phoneNumber?: string;
    status: string;
}

interface ICallCardState {
    callDuration: number;
}

// TODO: This does not belong to this component!

// export type CallStatusMode = "active" | "calling" | "idle" | "incoming-call";

export enum CallStatusMode {
    Active = "active",
    Calling = "calling",
    Idle = "idle",
    IncomingCall = "incoming-call"
}

@CSSModules(require<any>("./CallCard.css"))
export default class CallCard extends React.Component<ICallCardProperties, ICallCardState> {

    private static readonly defaultProps: Partial<ICallCardProperties> = {
        name: "unknown caller",
        phoneNumber: undefined
    };

    private readonly callCardStyle: any = {
        background: "none",
        borderRadius: 5,
        color: fullWhite,
        height: 372,
        padding: 10,
        position: "relative"
    };

    private randomContact: any;
    private timerId: number;

    public constructor() {
        super();

        this.state = {
            callDuration: 0
        };

        this.randomContact = _.sample(CONTACTS);
    }

    public render(): JSX.Element {
        const callCardContainerStyle = this.randomContact && {
            backgroundImage: `url(${this.randomContact.picture.src})`
        };

        const fooElement = this.props.status === "active" && (
            <div styleName="timer">
                {DateTime.formatSecondsToTimer(this.state.callDuration)}
            </div>
        );

        // TODO: Rename this variables!

        const bar = _.startCase(this.randomContact !== undefined ? this.randomContact.name : this.props.name);

        const qux = this.props.phoneNumber && (
            <div styleName="phone-number">
                {this.props.phoneNumber}
            </div>
        );

        return (
            <div
                styleName="container"
                style={callCardContainerStyle}>
                <div styleName="overlay">
                    <Paper
                        zDepth={1}
                        style={this.callCardStyle}>
                        <div styleName="details">
                            <div styleName="status">
                                {_.startCase(this.props.status)}
                            </div>
                            {fooElement}
                            <div styleName="contact">
                                <div styleName="name">
                                    {bar}
                                </div>
                                {qux}
                            </div>
                        </div>
                    </Paper>
                </div>
            </div>
        );
    }

    public componentWillReceiveProps(nextProps: ICallCardProperties): void {
        if (this.props.status !== "active" && nextProps.status === "active") {
            this.timerId = setInterval(() => {
                this.setState({
                    callDuration: this.state.callDuration + 1
                });
            }, 1000);
        }
    }

    public componentWillUnmount(): void {
        clearInterval(this.timerId);
    }

}
