import * as React from "react";

import { shallow } from "enzyme";

jest.mock("constants/data/Contacts", (): any => {
    return [];
});

import CallCard from "components/dialer/call-card/CallCard";

describe("CallCard", () => {

    jest.useFakeTimers();

    describe("#render", () => {

        test("call card with status renders correctly", () => {
            expect(shallow(<CallCard status="incoming-call" />)).toMatchSnapshot();
        });

        test("call card with name and status renders correctly", () => {
            expect(shallow(
                <CallCard
                    name="Tony Stark"
                    status="incoming-call" />
            )).toMatchSnapshot();
        });

        test("call card with name, phone number and status renders correctly", () => {
            expect(shallow(
                <CallCard
                    name="Groot"
                    phoneNumber="(866) 740-4531"
                    status="calling" />
            )).toMatchSnapshot();
        });

        test("call card with name, phone number, status and timer renders correctly", () => {
            const callCard = shallow(
                <CallCard
                    name="Groot"
                    phoneNumber="(866) 740-4531"
                    status="active" />
            );

            callCard.setState({
                callDuration: 345
            });

            expect(callCard).toMatchSnapshot();
        });

    });

    describe("@timer", () => {

        let callCard: any;

        beforeEach(() => {
            callCard = shallow(<CallCard status="incoming-call" />);

            callCard.setProps({
                status: "active"
            });
        });

        describe("#componentWillUpdate", () => {

            test("call duration timer is initialized with 1 second interval", () => {
                expect((setInterval as any).mock.calls.length).toEqual(1);
                expect((setInterval as any).mock.calls[0][1]).toEqual(1000);
            });

        });

        describe("#componentWillUnmount", () => {

            test("call duration timer is canceled", () => {
                const timerId = callCard.instance().timerId;

                callCard.unmount();

                expect((clearInterval as any).mock.calls.length).toEqual(1);
                expect((clearInterval as any).mock.calls[0][0]).toEqual(timerId);
            });

        });

        test("call duration timer updates state with each tick", () => {
            expect(callCard.state().callDuration).toEqual(0);

            jest.runTimersToTime(5000);

            expect(callCard.state().callDuration).toEqual(5);
        });

    });

});
