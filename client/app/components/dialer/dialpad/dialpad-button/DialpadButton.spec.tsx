import * as React from "react";

import { shallow } from "enzyme";

import FlatButton from "material-ui/FlatButton";

import DialpadButton from "components/dialer/dialpad/dialpad-button/DialpadButton";

describe("DialpadButton", () => {

    describe("#render", () => {

        test("button with main and label icons renders correctly", () => {
            expect(shallow(
                <DialpadButton
                    iconType="one"
                    labelType="voicemail"
                    onClick={undefined} />
            )).toMatchSnapshot();
        });

        test("button with main icon and text label renders correctly", () => {
            expect(shallow(
                <DialpadButton
                    iconType="two"
                    onClick={undefined}>
                    ABC
                </DialpadButton>
            )).toMatchSnapshot();
        });

        test("button with main icon of different size renders correctly", () => {
            expect(shallow(
                <DialpadButton
                    iconType="star"
                    iconSize={28}
                    onClick={undefined} />
            )).toMatchSnapshot();
        });

    });

    describe("#onTouchTap", () => {

        test("button 'one' event handler was called with expected arguments", () => {
            const onClickStub = jest.fn();

            const dialpadButton = shallow(
                <DialpadButton
                    iconType="one"
                    onClick={onClickStub} />
            ).find(FlatButton);

            dialpadButton.simulate("touchTap");

            expect(onClickStub).toHaveBeenCalledWith("1");
        });

        test("button 'two' event handler was called with expected arguments", () => {
            const onClickStub = jest.fn();

            const dialpadButton = shallow(
                <DialpadButton
                    iconType="two"
                    onClick={onClickStub} />
            ).find(FlatButton);

            dialpadButton.simulate("touchTap");

            expect(onClickStub).toHaveBeenCalledWith("2");
        });

        test("button 'three' event handler was called with expected arguments", () => {
            const onClickStub = jest.fn();

            const dialpadButton = shallow(
                <DialpadButton
                    iconType="three"
                    onClick={onClickStub} />
            ).find(FlatButton);

            dialpadButton.simulate("touchTap");

            expect(onClickStub).toHaveBeenCalledWith("3");
        });

        test("button 'four' event handler was called with expected arguments", () => {
            const onClickStub = jest.fn();

            const dialpadButton = shallow(
                <DialpadButton
                    iconType="four"
                    onClick={onClickStub} />
            ).find(FlatButton);

            dialpadButton.simulate("touchTap");

            expect(onClickStub).toHaveBeenCalledWith("4");
        });

        test("button 'five' event handler was called with expected arguments", () => {
            const onClickStub = jest.fn();

            const dialpadButton = shallow(
                <DialpadButton
                    iconType="five"
                    onClick={onClickStub} />
            ).find(FlatButton);

            dialpadButton.simulate("touchTap");

            expect(onClickStub).toHaveBeenCalledWith("5");
        });

        test("button 'six' event handler was called with expected arguments", () => {
            const onClickStub = jest.fn();

            const dialpadButton = shallow(
                <DialpadButton
                    iconType="six"
                    onClick={onClickStub} />
            ).find(FlatButton);

            dialpadButton.simulate("touchTap");

            expect(onClickStub).toHaveBeenCalledWith("6");
        });

        test("button 'seven' event handler was called with expected arguments", () => {
            const onClickStub = jest.fn();

            const dialpadButton = shallow(
                <DialpadButton
                    iconType="seven"
                    onClick={onClickStub} />
            ).find(FlatButton);

            dialpadButton.simulate("touchTap");

            expect(onClickStub).toHaveBeenCalledWith("7");
        });

        test("button 'eight' event handler was called with expected arguments", () => {
            const onClickStub = jest.fn();

            const dialpadButton = shallow(
                <DialpadButton
                    iconType="eight"
                    onClick={onClickStub} />
            ).find(FlatButton);

            dialpadButton.simulate("touchTap");

            expect(onClickStub).toHaveBeenCalledWith("8");
        });

        test("button 'nine' event handler was called with expected arguments", () => {
            const onClickStub = jest.fn();

            const dialpadButton = shallow(
                <DialpadButton
                    iconType="nine"
                    onClick={onClickStub} />
            ).find(FlatButton);

            dialpadButton.simulate("touchTap");

            expect(onClickStub).toHaveBeenCalledWith("9");
        });

        test("button 'star' event handler was called with expected arguments", () => {
            const onClickStub = jest.fn();

            const dialpadButton = shallow(
                <DialpadButton
                    iconType="star"
                    onClick={onClickStub} />
            ).find(FlatButton);

            dialpadButton.simulate("touchTap");

            expect(onClickStub).toHaveBeenCalledWith("*");
        });

        test("button 'zero' event handler was called with expected arguments", () => {
            const onClickStub = jest.fn();

            const dialpadButton = shallow(
                <DialpadButton
                    iconType="zero"
                    onClick={onClickStub} />
            ).find(FlatButton);

            dialpadButton.simulate("touchTap");

            expect(onClickStub).toHaveBeenCalledWith("0");
        });

        test("button 'hash' event handler was called with expected arguments", () => {
            const onClickStub = jest.fn();

            const dialpadButton = shallow(
                <DialpadButton
                    iconType="hash"
                    onClick={onClickStub} />
            ).find("FlatButton");

            dialpadButton.simulate("touchTap");

            expect(onClickStub).toHaveBeenCalledWith("#");
        });

    });

});
