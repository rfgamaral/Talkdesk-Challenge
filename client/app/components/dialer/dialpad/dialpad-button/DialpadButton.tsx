import * as React from "react";
import * as CSSModules from "react-css-modules";

import FlatButton from "material-ui/FlatButton";
import FontIcon from "material-ui/FontIcon";
import Paper from "material-ui/Paper";

import { grey100, grey200, lightBlue600, lightBlue800 } from "material-ui/styles/colors";

import { DIALPAD_DIGITS } from "constants/dialer/DialpadDigits";

interface IDialpadButtonProperties {
    iconSize?: number;
    iconType: string;
    labelType?: string;
    onClick(digit: string): void;
}

@CSSModules(require<any>("./DialpadButton.css"))
export default class DialpadButton extends React.Component<IDialpadButtonProperties, any> {

    private static readonly defaultProps: Partial<IDialpadButtonProperties> = {
        iconSize: 44,
        labelType: undefined
    };

    private readonly dialpadButtonStyle: any = {
        height: 64,
        width: "100%"
    };

    private readonly dialpadButtonIconStyle: any = {
        color: lightBlue600,
        fontSize: 0
    };

    private readonly dialpadButtonItemStyle: any = {
        color: lightBlue800
    };

    public constructor(props: IDialpadButtonProperties) {
        super(props);

        this.dialpadButtonIconStyle.fontSize = props.iconSize;

        this.handleButtonTouchTap = this.handleButtonTouchTap.bind(this);
    }

    public render(): JSX.Element {
        const dialpadButtonLabelElement = this.props.labelType === undefined ? this.props.children : (
            <FontIcon
                className={`icon-dialpad-${this.props.labelType}`}
                color={this.dialpadButtonItemStyle.color} />
        );

        return (
            <Paper zDepth={1} style={this.dialpadButtonStyle}>
                <FlatButton
                    backgroundColor={grey100}
                    hoverColor={grey200}
                    onTouchTap={this.handleButtonTouchTap}
                    style={this.dialpadButtonStyle}>
                    <div styleName={`container${this.props.children === undefined ? "-single" : ""}`}>
                        <div styleName={`item${this.props.children === undefined ? "-left" : ""}`}>
                            <FontIcon
                                className={`icon-dialpad-${this.props.iconType}`}
                                style={this.dialpadButtonIconStyle} />
                        </div>
                        <div styleName="item-right" style={this.dialpadButtonItemStyle}>
                            {dialpadButtonLabelElement}
                        </div>
                    </div>
                </FlatButton>
            </Paper>
        );
    }

    private handleButtonTouchTap(event: any): void {
        this.props.onClick(DIALPAD_DIGITS[this.props.iconType]);
    }

}
