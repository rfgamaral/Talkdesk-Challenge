import * as React from "react";
import * as CSSModules from "react-css-modules";

import DialpadButton from "./dialpad-button/DialpadButton";

interface IDialpadProperties {
    onDial(digit: string): void;
}

@CSSModules(require<any>("./Dialpad.css"))
export default class Dialpad extends React.PureComponent<IDialpadProperties, any> {

    public constructor() {
        super();

        this.handleAnyButtonClick = this.handleAnyButtonClick.bind(this);
    }

    public render(): JSX.Element {
        return (
            <div styleName="container">
                <div styleName="row">
                    <div styleName="cell">
                        <DialpadButton
                            onClick={this.handleAnyButtonClick}
                            iconType="one"
                            labelType="voicemail" />
                    </div>
                    <div styleName="cell">
                        <DialpadButton
                            onClick={this.handleAnyButtonClick}
                            iconType="two">
                            ABC
                        </DialpadButton>
                    </div>
                    <div styleName="cell">
                        <DialpadButton
                            onClick={this.handleAnyButtonClick}
                            iconType="three">
                            DEF
                        </DialpadButton>
                    </div>
                </div>
                <div styleName="row">
                    <div styleName="cell">
                        <DialpadButton
                            onClick={this.handleAnyButtonClick}
                            iconType="four">
                            GHI
                        </DialpadButton>
                    </div>
                    <div styleName="cell">
                        <DialpadButton
                            onClick={this.handleAnyButtonClick}
                            iconType="five">
                            JKL
                        </DialpadButton>
                    </div>
                    <div styleName="cell">
                        <DialpadButton
                            onClick={this.handleAnyButtonClick}
                            iconType="six">
                            MNO
                        </DialpadButton>
                    </div>
                </div>
                <div styleName="row">
                    <div styleName="cell">
                        <DialpadButton
                            onClick={this.handleAnyButtonClick}
                            iconType="seven">
                            PQRS
                        </DialpadButton>
                    </div>
                    <div styleName="cell">
                        <DialpadButton
                            onClick={this.handleAnyButtonClick}
                            iconType="eight">
                            TUV
                        </DialpadButton>
                    </div>
                    <div styleName="cell">
                        <DialpadButton
                            onClick={this.handleAnyButtonClick}
                            iconType="nine">
                            WXYZ
                        </DialpadButton>
                    </div>
                </div>
                <div styleName="row">
                    <div styleName="cell">
                        <DialpadButton
                            onClick={this.handleAnyButtonClick}
                            iconType="star"
                            iconSize={28} />
                    </div>
                    <div styleName="cell">
                        <DialpadButton
                            onClick={this.handleAnyButtonClick}
                            iconType="zero">
                            +
                        </DialpadButton>
                    </div>
                    <div styleName="cell">
                        <DialpadButton
                            onClick={this.handleAnyButtonClick}
                            iconType="hash"
                            iconSize={28} />
                    </div>
                </div>
            </div>
        );
    }

    private handleAnyButtonClick(digit: string): void {
        this.props.onDial(digit);
    }

}
