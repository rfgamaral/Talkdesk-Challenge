import * as React from "react";

import { shallow } from "enzyme";

import Dialpad from "components/dialer/dialpad/Dialpad";
import DialpadButton from "components/dialer/dialpad/dialpad-button/DialpadButton";

describe("Dialpad", () => {

    describe("#render", () => {

        test("dialpad renders correctly", () => {
            expect(shallow(
                <Dialpad
                    onDial={undefined} />
            )).toMatchSnapshot();
        });

    });

    describe("#onTouchTap", () => {

        const onDialStub = jest.fn();

        const dialpadButtons = shallow(
            <Dialpad onDial={onDialStub} />
        ).find(DialpadButton);

        test("button '1' event handler was called with expected arguments", () => {
            dialpadButtons.at(0).simulate("click", "1");
            expect(onDialStub).toHaveBeenCalledWith("1");
        });

        test("button '2' event handler was called with expected arguments", () => {
            dialpadButtons.at(1).simulate("click", "2");
            expect(onDialStub).toHaveBeenCalledWith("2");
        });

        test("button '3' event handler was called with expected arguments", () => {
            dialpadButtons.at(2).simulate("click", "3");
            expect(onDialStub).toHaveBeenCalledWith("3");
        });

        test("button '4' event handler was called with expected arguments", () => {
            dialpadButtons.at(3).simulate("click", "4");
            expect(onDialStub).toHaveBeenCalledWith("4");
        });

        test("button '5' event handler was called with expected arguments", () => {
            dialpadButtons.at(4).simulate("click", "5");
            expect(onDialStub).toHaveBeenCalledWith("5");
        });

        test("button '6' event handler was called with expected arguments", () => {
            dialpadButtons.at(5).simulate("click", "6");
            expect(onDialStub).toHaveBeenCalledWith("6");
        });

        test("button '7' event handler was called with expected arguments", () => {
            dialpadButtons.at(6).simulate("click", "7");
            expect(onDialStub).toHaveBeenCalledWith("7");
        });

        test("button '8' event handler was called with expected arguments", () => {
            dialpadButtons.at(7).simulate("click", "8");
            expect(onDialStub).toHaveBeenCalledWith("8");
        });

        test("button '9' event handler was called with expected arguments", () => {
            dialpadButtons.at(8).simulate("click", "9");
            expect(onDialStub).toHaveBeenCalledWith("9");
        });

        test("button '*' event handler was called with expected arguments", () => {
            dialpadButtons.at(9).simulate("click", "*");
            expect(onDialStub).toHaveBeenCalledWith("*");
        });

        test("button '0' event handler was called with expected arguments", () => {
            dialpadButtons.at(10).simulate("click", "0");
            expect(onDialStub).toHaveBeenCalledWith("0");
        });

        test("button '#' event handler was called with expected arguments", () => {
            dialpadButtons.at(11).simulate("click", "#");
            expect(onDialStub).toHaveBeenCalledWith("#");
        });

    });

});
