import * as _ from "lodash";
import * as React from "react";

import EventListener from "react-event-listener";

import { shallow } from "enzyme";

import CallButton from "components/dialer/call-button/CallButton";
import Dialer from "components/dialer/Dialer";

import { KeyCodes } from "constants/events/KeyCodes";

describe("Dialer", () => {

    let dialer: any;
    let eventListener: any;

    const simulateKeyDown = (event: any): void => {
        eventListener.simulate("keyDown", {
            key: event.key,
            keyCode: event.keyCode
        });
    };

    beforeAll(() => {
        global.Twilio = {
             Device: {
                 cancel: jest.fn(),
                 connect: jest.fn(),
                 disconnect: jest.fn(),
                 incoming: jest.fn()
             }
        };
    });

    beforeEach(() => {
         dialer = shallow(<Dialer />);
         eventListener = dialer.find(EventListener);
    });

    describe("#render", () => {

        test("dialer in 'idle' mode renders correctly", () => {
            expect(dialer).toMatchSnapshot();
        });

        test("dialer in 'idle' mode with input phone number renders correctly", () => {
            dialer.setState({
                phoneNumber: "(866) 740-4531"
            });

            expect(dialer).toMatchSnapshot();
        });

        test("dialer in 'calling' mode renders correctly", () => {
            dialer.setState({
                callStatus: "calling"
            });

            expect(dialer).toMatchSnapshot();
        });

        test("dialer in 'incoming call' mode renders correctly", () => {
            dialer.setState({
                callStatus: "incoming-call"
            });

            expect(dialer).toMatchSnapshot();
        });

        test("dialer in 'active' mode renders correctly", () => {
            dialer.setState({
                callStatus: "active"
            });

            expect(dialer).toMatchSnapshot();
        });

        test("dialer in 'active' mode with card name and phone number renders correctly", () => {
            dialer.setState({
                callStatus: "active",
                cardName: "Groot",
                cardPhoneNumber: "(866) 740-4531"
            });

            expect(dialer).toMatchSnapshot();
        });

    });

    // Tests below are failing because they were not fixed to work with Twilio

    describe.skip("#documentKeyDown", () => {

        describe("@keyboard", () => {

            test("pressing 'enter' with dialer in 'idle' mode without a phone number does nothing", () => {
                dialer.setState({
                    phoneNumber: ""
                });

                simulateKeyDown({ keyCode: KeyCodes.Enter });

                expect(dialer.state().callStatus).toEqual("idle");
            });

            test("pressing 'enter' with dialer in 'idle' mode with a phone number switches to 'calling' mode", () => {
                dialer.setState({
                    phoneNumber: "(866) 740-4531"
                });

                simulateKeyDown({ keyCode: KeyCodes.Enter });

                expect(dialer.state().callStatus).toEqual("calling");
                expect(dialer.state().cardPhoneNumber).toEqual("(866) 740-4531");
            });

            test("pressing 'enter' with dialer in 'incoming call' mode switches to 'active' mode", () => {
                dialer.setState({
                    callStatus: "incoming-call"
                });

                simulateKeyDown({ keyCode: KeyCodes.Enter });

                expect(dialer.state().callStatus).toEqual("active");
            });

            test("pressing 'esc' with dialer in 'active' mode switches to 'idle' mode", () => {
                dialer.setState({
                    callStatus: "active"
                });

                simulateKeyDown({ keyCode: KeyCodes.Esc });

                expect(dialer.state().callStatus).toEqual("idle");
            });

            test("pressing 'esc' with dialer in 'calling' mode switches to 'idle' mode", () => {
                dialer.setState({
                    callStatus: "calling"
                });

                simulateKeyDown({ keyCode: KeyCodes.Esc });

                expect(dialer.state().callStatus).toEqual("idle");
            });

            test("pressing 'esc' with dialer in 'incoming call' mode switches to 'idle' mode", () => {
                dialer.setState({
                    callStatus: "incoming-call"
                });

                simulateKeyDown({ keyCode: KeyCodes.Esc });

                expect(dialer.state().callStatus).toEqual("idle");
            });

        });

        describe("@dialpad", () => {

            describe("with dialer in 'idle' mode", () => {

                test("pressing 'backspace' without a phone number does nothing to the phone number", () => {
                    dialer.setState({
                        phoneNumber: ""
                    });

                    simulateKeyDown({ keyCode: KeyCodes.Backspace });

                    expect(dialer.state().phoneNumber).toEqual("");
                });

                test("pressing 'backspace' with a phone number deletes the last digit from the phone number", () => {
                    dialer.setState({
                        phoneNumber: "(866) 740-4531"
                    });

                    simulateKeyDown({ keyCode: KeyCodes.Backspace });

                    expect(dialer.state().phoneNumber).toEqual("(866) 740-453");
                });

                test("pressing an invalid dialpad key does nothing to the phone number", () => {
                    dialer.setState({
                        phoneNumber: "123"
                    });

                    simulateKeyDown({ key: "x" });

                    expect(dialer.state().phoneNumber).toEqual("123");
                });

                test("pressing a valid dialpad key with adds the digit to the phone number", () => {
                    spyOn(_, "map").and.returnValue(["5"]);

                    dialer.setState({
                        phoneNumber: "1234"
                    });

                    simulateKeyDown({ key: "5" });

                    expect(dialer.state().phoneNumber).toEqual("12345");
                });

            });

        });

    });

    // Tests below are failing because they were not fixed to work with Twilio

    describe.skip("@callButton", () => {

        test("pressing 'answer' with dialer in 'incoming-call' mode switches to 'active' mode", () => {
            dialer.setState({
                callStatus: "incoming-call"
            });

            const callButtons = dialer.find(CallButton);

            const answerCallButton = callButtons.find({
                type: "answer"
            });

            answerCallButton.simulate("click", answerCallButton.props().type);

            expect(dialer.state().callStatus).toEqual("active");
        });

        test("pressing 'decline' with dialer in 'incoming-call' mode switches to 'idle' mode", () => {
            dialer.setState({
                callStatus: "incoming-call"
            });

            const callButtons = dialer.find(CallButton);

            const declineCallButton = callButtons.find({
                type: "decline"
            });

            declineCallButton.simulate("click", declineCallButton.props().type);

            expect(dialer.state().callStatus).toEqual("idle");
        });

        test("pressing 'end call' with dialer in 'active' mode switches to 'idle' mode", () => {
            dialer.setState({
                callStatus: "active"
            });

            const endCallCallButton = dialer.find(CallButton);

            endCallCallButton.simulate("click", endCallCallButton.props().type);

            expect(dialer.state().callStatus).toEqual("idle");
        });

        test("pressing 'end call' with dialer in 'calling' mode switches to 'idle' mode", () => {
            dialer.setState({
                callStatus: "calling"
            });

            const endCallCallButton = dialer.find(CallButton);

            endCallCallButton.simulate("click", endCallCallButton.props().type);

            expect(dialer.state().callStatus).toEqual("idle");
        });

        test("pressing 'call' with dialer in 'idle' mode without a phone number does nothing", () => {
            const callCallButton = dialer.find(CallButton);

            callCallButton.simulate("click", callCallButton.props().type);

            expect(dialer.state().callStatus).toEqual("idle");
        });

        test("pressing 'call' with dialer in 'idle' mode with a phone number switches to 'calling' mode", () => {
            dialer.setState({
                phoneNumber: "(866) 740-4531"
            });

            const callCallButton = dialer.find(CallButton);

            callCallButton.simulate("click", callCallButton.props().type);

            expect(dialer.state().callStatus).toEqual("calling");
        });

    });

});
