import * as anthonyStark from "assets/images/profiles/anthony-stark.jpg";
import * as clintonBarton from "assets/images/profiles/clinton-barton.jpg";
import * as henryPym from "assets/images/profiles/henry-pym.jpg";
import * as jamesHowlett from "assets/images/profiles/james-howlett.jpg";
import * as matthewMurdock from "assets/images/profiles/matthew-murdock.jpg";
import * as natashaRomanoff from "assets/images/profiles/natasha-romanoff.jpg";
import * as peterParker from "assets/images/profiles/peter-parker.jpg";
import * as pietroMaximoff from "assets/images/profiles/pietro-maximoff.jpg";
import * as stevenRogers from "assets/images/profiles/steven-rogers.jpg";
import * as thorOdison from "assets/images/profiles/thor-odison.jpg";
import * as victorShade from "assets/images/profiles/victor-shade.jpg";
import * as wandaMaximoff from "assets/images/profiles/wanda-maximoff.jpg";

/**
 * Collection of mocked contacts with pictures for demonstration purposes only.
 */
export const CONTACTS: Array<{ name: string, picture: any}> = [
    {
        name: "Anthony Stark",
        picture: anthonyStark
    },
    {
        name: "Clinton Barton",
        picture: clintonBarton
    },
    {
        name: "Henry Pym",
        picture: henryPym
    },
    {
        name: "James Howlett",
        picture: jamesHowlett
    },
    {
        name: "Matthew Murdock",
        picture: matthewMurdock
    },
    {
        name: "Natasha Romanoff",
        picture: natashaRomanoff
    },
    {
        name: "Peter Parker",
        picture: peterParker
    },
    {
        name: "Pietro Maximoff",
        picture: pietroMaximoff
    },
    {
        name: "Steven Rogers",
        picture: stevenRogers
    },
    {
        name: "Thor Odison",
        picture: thorOdison
    },
    {
        name: "Victor Shade",
        picture: victorShade
    },
    {
        name: "Wanda Maximoff",
        picture: wandaMaximoff
    }
];
