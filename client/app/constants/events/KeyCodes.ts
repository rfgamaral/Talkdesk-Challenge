/**
 * Enumeration of key codes for common characters.
 */
export enum KeyCodes {
    Backspace = 8,
    Enter = 13,
    Esc = 27
}
