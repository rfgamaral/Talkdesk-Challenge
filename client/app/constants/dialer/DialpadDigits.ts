/**
 * Collection of valid digits for a dialpad.
 */
export const DIALPAD_DIGITS: { [digit: string]: string; } = {
    /* tslint:disable:object-literal-sort-keys */
    one: "1",
    two: "2",
    three: "3",
    four: "4",
    five: "5",
    six: "6",
    seven: "7",
    eight: "8",
    nine: "9",
    star: "*",
    zero: "0",
    hash: "#",
    plus: "+"
    /* tslint:enable:object-literal-sort-keys */
};
