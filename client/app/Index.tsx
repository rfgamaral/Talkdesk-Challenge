import * as React from "react";
import * as ReactDOM from "react-dom";

import { AppContainer } from "react-hot-loader";
import * as injectTapEventPlugin from "react-tap-event-plugin";

import Application from "./components/Application";

import "./assets/styles/theme.css";

/**
 * Inject the tap event plugin (required by Material-UI) before rendering the application.
 */
injectTapEventPlugin();

/**
 * Render the main application component into the DOM in the supplied HMR container.
 */
// tslint:disable-next-line:variable-name
(function handleHotModuleReload(Component: typeof Application, isFirstRender: boolean = true): void {
    ReactDOM.render(
        <AppContainer>
            <Component />
        </AppContainer>,
        document.getElementById("root")
    );

    if (isFirstRender && module.hot) {
        module.hot.accept("./components/Application", () => {
            handleHotModuleReload(require<any>("./components/Application").default, false);
        });
    }
})(Application);
