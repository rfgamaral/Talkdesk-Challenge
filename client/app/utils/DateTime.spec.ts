import * as DateTime from "utils/DateTime";

describe("DateTime", () => {

    describe("#formatSecondsToTimer", () => {

        test("negative number of seconds throws error", () => {
            expect(() => {
                DateTime.formatSecondsToTimer(-1);
            }).toThrowError("Expected a non-negative integer, got '-1'.");
        });

        test("zero seconds returns expected result", () => {
            expect(DateTime.formatSecondsToTimer(0)).toEqual("00:00");
        });

        test("number of seconds without decimals returns expected result", () => {
            expect(DateTime.formatSecondsToTimer(256)).toEqual("04:16");
        });

        test("number of seconds with decimals returns expected result", () => {
            expect(DateTime.formatSecondsToTimer(256.123)).toEqual("04:16");
        });

        test("number of seconds higher than 100mins without decimals returns expected result", () => {
            expect(DateTime.formatSecondsToTimer(8192)).toEqual("136:32");
        });

        test("number of seconds higher than 100mins with decimals returns expected result", () => {
            expect(DateTime.formatSecondsToTimer(8192.987)).toEqual("136:32");
        });

    });

});
