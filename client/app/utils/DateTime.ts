/**
 * Format the given number of seconds to a timer with the `MM:SS` format.
 *
 * @param {number} seconds The number of seconds to format.
 * @return {string} The number of seconds with the `MM:SS` format.
 *
 * @throws Will throw an error if `seconds` is lower than `0`.
 */
export function formatSecondsToTimer(seconds: number): string {
    if (seconds < 0) {
        throw new Error(`Expected a non-negative integer, got '${seconds}'.`);
    }

    const mm = Math.floor(seconds / 60).toString();
    const ss = (Math.floor(seconds) % 60).toString();

    return ("00" + mm).slice(-Math.max(2, mm.length)) + ":" +
        ("00" + ss).slice(-Math.max(2, ss.length));
}
